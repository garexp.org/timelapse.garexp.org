#!/bin/bash

lftp \
    -e "set ssl:verify-certificate false; mirror -R --no-perms --exclude='.git' --exclude='$( basename $0)' ; quit" \
    ftp.garexp15.o2switch.net
